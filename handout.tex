\documentclass[a4paper,10pt,DIV=59,ngerman,headlines=0]{scrartcl}
%\usepackage{plex-serif}
\usepackage[table,dvipsnames]{xcolor}
\usepackage{wrapfig}
\usepackage{graphicx}
\usepackage{amsmath,amsfonts,amssymb,amsthm}
\usepackage{siunitx}
\usepackage{tikz,pgfplots}
\pgfplotsset{compat=newest}
\usepackage{color, colortbl}
\usepackage{caption}
\usepackage{subcaption}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{bold-extra}
\usepackage{gensymb}
\usepackage{mdframed}
\usepackage{enumitem}
\usepackage{mathtools}
\usetikzlibrary{decorations.pathreplacing}
\usetikzlibrary{plotmarks}
\usetikzlibrary{shapes.misc}
\usetikzlibrary{positioning}
\newtheorem{theorem}{Theorem}[]
\newtheorem*{theorem*}{Theorem}
\newtheorem{proposition}{Proposition}[]
\newtheorem{kor}{Korollar}[]
\newtheorem{lemma}[]{Lemma}
\theoremstyle{definition}
\newtheorem*{definition*}{Definition}

\tikzset{%
  show curve controls/.style={
    postaction={
      decoration={
        show path construction,
        curveto code={
          \draw [blue] 
            (\tikzinputsegmentfirst) -- (\tikzinputsegmentsupporta)
            (\tikzinputsegmentlast) -- (\tikzinputsegmentsupportb);
          \fill [red, opacity=0.5] 
            (\tikzinputsegmentsupporta) circle [radius=.5ex]
            (\tikzinputsegmentsupportb) circle [radius=.5ex];
        }
      },
      decorate
}}}


\tikzset{cross/.style={cross out, draw=black, minimum size=2*(#1-\pgflinewidth), inner sep=0pt, outer sep=0pt},
%default radius will be 1pt. 
cross/.default={1pt}}

\usepackage[german]{babel}
\usepackage[outline]{contour}
\usepackage{xfrac}

\addtokomafont{disposition}{\rmfamily}
\addtokomafont{section}{\Large\selectfont\bfseries}
\addtokomafont{subsection}{\large\selectfont}

\newcommand{\dx}{\text{\normalfont{ d}}}
\newcommand{\TD}{\text{\normalfont{D}}}
\newcommand{\R}{\mathbb{R}}


\linespread{1}

\newcommand{\mat}[1] {
	\begin{bmatrix} #1 \end{bmatrix}
}

\title{Autonomes Fahren}
\subtitle{Abschlussprojekt}
\date{}
\author{\normalsize{Jonas Sattler}}

\begin{document}
\setlength{\abovedisplayskip}{2pt}
\setlength{\belowdisplayskip}{1pt}
\setlength{\abovedisplayshortskip}{3pt}
\setlength{\belowdisplayshortskip}{3pt}
\pagenumbering{gobble}
    %\maketitle
    \begin{center}
        \raisebox{-4pt}{\rule{1\textwidth}{2pt}}
        \vspace{0em}
        \LARGE\textbf{\textsc{Alexandroff-Bakelman-Pucci Abschätzung}}\\
        \large\textsc{Teil 2}\\
        {\textit{Jonas Sattler}}
        \raisebox{0.66em}{\rule{1\textwidth}{2pt}}
    \end{center}
    \vspace{-1em}

\global\mdfdefinestyle{exampledefault}{%
linecolor=black,linewidth=1pt,%
leftmargin=0cm,rightmargin=0cm,rightline=true,innerleftmargin=10,innerrightmargin=10,
frametitlerule=true,frametitlerulecolor=black,
frametitlebackgroundcolor=white,
frametitlerulewidth=1pt
}
\begin{mdframed}[style=exampledefault, frametitle={\large{Erinnerungen und Voraussetzungen}}]
    \begin{minipage}[c]{0.65\textwidth}
        \begin{definition*}[Konvexe Hülle]
            Sei $v$ eine stetige Funktion auf einer offenen und konvexen Menge $A$, dann ist die \emph{konvexe Hülle} von $v$ definiert als
            \begin{align*}
                \Gamma_v(x) &= \sup_{w}\{w(x) : w \leq v \text{ in } A \text{, } w \text{ konvex} \}\\
                &= \sup_{L}\{L(x) : L \leq v \text{ in } A \text{, } L \text{ affin linear} \}.
            \end{align*}
            $\Gamma_v$ ist also eine konvexe Funktion auf $A$. Die Menge $\{v = \Gamma_v\}$ heißt \emph{Kontaktmenge} und ihre Elemente \emph{Kontaktpunkte}.
        \end{definition*}
    \end{minipage}
    \begin{minipage}[c]{0.35\textwidth}
        \begin{center}
            \begin{tikzpicture}[scale=1.5]
                \draw [thick] (0, 0) 
                  .. controls (0.5,-1) and ++(-0.25,0) .. ( 1, -1)
                  .. controls ++(0.5,0) and ++(-0.5,0) .. ( 2, 0.25)
                  .. controls ++(0.5,0) and ++(-0.5,-0.1) .. ( 3, 0)
                  .. controls ++(0.25,0.1) and ++(-0.1,-0.25) .. ( 3.5, 0.5);

                \draw [red, dashed, very thick] (0, 0) 
                  .. controls (0.5,-1) and +(-0.25,0) .. ( 1, -1)
                  .. controls ++(0.1,0) and (1,-1) .. ( 3, 0)
                  .. controls ++(0.25,0.1) and ++(-0.1,-0.25) .. ( 3.5, 0.5);

              \draw [thick] (2.75,-0.5) -- (3,-0.5) node[right, black] {: $v$};
              \draw [red, dashed, very thick] (2.75,-0.75) -- (3,-0.75) node[right, black] {: $\Gamma_v$};
            \end{tikzpicture}
        \end{center}
    \end{minipage}
    \begin{theorem*}[Satz von Carathéodory]
        Sei $x$ ein Punkt in der konvexen Hülle einer Menge $M \subset \R^{n}$, dann lässt sich $x$ als Konvexkombination von $n+1$ Punkten von $M$ schreiben. Also:
        \[x = \sum_{i=1}^{n+1} \lambda_{i} x_{i}, \quad \sum_{i=1}^{n+1} \lambda_{i} = 1, \quad \lambda_{i} \geq 0 \quad \forall i \in \{1,...,n+1\}.\]
        Äquivalent hierzu ist, dass sich $x_{0}$ in einem Simplex mit Kanten $x_{1},...,x_{n+1}$ befindet.
    \end{theorem*}
    \begin{proposition}
        Seien $\Omega, \Omega_{1} \subset \R^{n}$ beschränkte Gebiete mit $\overline{\Omega} \subset \Omega_{1}$. Seien $u \in C(\Omega_{1}), v \in C(\overline{\Omega})$, s.d. $F(\TD^{2}u(x),x) \leq f(x)$ auf $\Omega_{1}$ und $F(\TD^{2}v(x),x) \leq g(x)$ auf $\Omega$ im Viskositätssinne. Wir verlangen $v \geq u$ auf  $\partial \Omega$ und setzen
        \[w = \left\{
                \begin{array}{ll}
                    u & \text{in } \Omega_{1} \setminus \Omega\\
                    \min(u,v) & \text{in } \overline{\Omega}
                \end{array}
        \right.,
        \quad
        h = \left\{
            \begin{array}{ll}
                f & \text{in } \Omega_{1} \setminus \Omega\\
                \max(f,g) & \text{in } \overline{\Omega}
            \end{array}
        \right..
        \]
        Dann ist $w$ eine Viskositätssuperlösung von $F(\TD^{2}w(x),x) = h(x)$ in $\Omega_{1}$.
    \end{proposition}
    \begin{proposition}
        Sei $u$ eine stetige Funktion auf $\Omega$ und $M$ ein konvexes Gebiet mit $\overline{M} \subset \Omega$ und $\epsilon>0$ eine Konstante, so dass für alle $x_{0} \in \overline{M}$ sowohl eine konvexe, als auch eine konkave, Parabel mit Öffnungsparameter kleiner als $K$, die $u$ in $x_{0}$ von oben, bzw. von unten, auf $M \cap B_{\epsilon}(x_{0})$ berühren, existieren. Also, Parabeln $P_{1}(x), P_{2}(x)$, so dass
        \begin{align*}
            P_{1}(x) \leq u(x) \leq P_{2}(x) \quad \forall x \in M \cap B_{\epsilon}(x_{0}), \quad P_{1}(x_{0})=u(x_{0})=P_{2}(x_{0}).
        \end{align*}
        Dann ist $u \in C^{1,1}(\overline{M})$.
    \end{proposition}
    \begin{lemma}
        Sei $u \in \overline{S}(\lambda, \Lambda, f)$ auf $B_\delta$, $f:B_\delta \to \R$ eine beschränkte Funktion und $\varphi : B_\delta \to \R$ konvex mit $0 \leq \varphi \leq u$ auf $B_\delta$ und $0 = \varphi(0) = u(0)$. Dann gilt
        \[\varphi \leq C(n, \lambda, \Lambda)(\sup_{B_\delta}f^+) |x|^2 \quad \forall x \in B_{\nu \delta}\]
        mit einer Konstanten $\nu = \nu(n,\lambda, \Lambda)< 1$.
    \end{lemma}
    %\setcounter{lemma}{0}
    \begin{lemma}
        Sei $u$ eine stetige Funktion auf $\overline{B}_d$ mit $u \geq 0$ auf $\partial B_d$ und $\Gamma_u$ wie im Theorem über die ABP Abschätzung. Wenn $\Gamma_u \in C^{1,1}(\overline{B}_d)$, dann existiert eine Menge $A \subset B_d$, sodass $|B_d \setminus A| = 0$ und $\Gamma_u$ zweifach differenzierbar in allen $x \in A$ ist. Weiterhin gilt die Abschätzung
        \[\sup_{B_d}u^{-} \leq C(n) d \left( \int_A \det \TD^2 \Gamma_u \dx x \right)^{1/n}.\]
    \end{lemma}
\end{mdframed}

Ziel ist der Beweis der Alexandroff-Bakelman-Pucci (ABP) Abschätzung:
\begin{theorem*}[Alexandroff-Bakelman-Pucci Abschätzung]
    Sei $u \in \overline{S}(\lambda, \Lambda, f)$ auf $\overline{B}_d$ und $f: B_d \to \R$ eine stetige, beschränkte Funktion. Ist $u$ stetig auf $\overline{B}_d$ und $u \geq 0$ auf $\partial B_d$, dann gilt
    \[\sup_{B_d}u^{-} \leq C(n,\lambda, \Lambda) d \left( \int_{B_d \cap \{u = \Gamma_u\}} (f^+)^n \dx x \right)^{1/n}.\]
    Wobei u durch $0$ auf $B_{2d}$ fortgesetzt wird, sodass $-u^-$ stetig auf $B_{2d}$ ist. $\Gamma_{u}$ ist hier die konvexe Hülle von $-u^{-}$ auf $\overline{B}_{2d}$.
\end{theorem*}
Lemma 2 ähnelt bereits der gewünschten ABP Abschätzung, unterscheidet sich jedoch immer noch in zwei Punkten. Zunächst ist die integrierte Funktion $\det \TD^{2} \Gamma_{u}$ und nicht $(f^{+})^{n}$, aber auch die Mengen, über die integriert wird, unterscheiden sich. Letzterer Punkt wird im folgenden Lemma, einer Verfeinerung von Lemma 2, adressiert.

\begin{lemma}
Sei $u$ eine stetig Funktion in $\overline{B}_{d}$, sodass $u \geq 0$ auf $\partial B_{d}$ und $\Gamma _{u}$ definiert wie im obigen Theorem. Weiterhin sei $K>0$ und $0 <  \epsilon \ll d$ Konstanten. Angenommen, dass für alle $x_{0} \in \overline{B}_{d} \cap \{u = \Gamma_{u}\}$ eine konvexe Parabel mit Öffnungsparameter $K$ existiert, der $\Gamma_{u}$ von oben bei $x_{0}$ auf $B_{\epsilon}(x_{0})$ berührt, also dass ein $P(x) = l_{0} + l(x) + \sfrac{K}{2}|x|^{2}$ mit
    \begin{align*}
        \Gamma_{u}(x) \leq P(x) \quad \forall x \in B_{\epsilon}(x_{0}), \quad \Gamma_{u}(x_{0}) = P(x_{0})
    \end{align*}
    existiert. Dann ist $\Gamma_{u} \in C^{1,1}(\overline{B}_{d})$ und somit existiert eine Menge $A \subset B$ sodass $|B_{d} \setminus A| = 0$ und $\Gamma_{u}$ zweifach differenzierbar auf $A$. Weiterhin gilt die Abschätzung
    \[\sup_{B_d}u^{-} \leq C(n) d \left( \int_{A \cap \{u = \Gamma_{u}\}} \det \TD^2 \Gamma_u \dx x \right)^{1/n}.\]
\end{lemma}

\begin{proof}[Beweisskizze]
    Vorüberlegung. Durch die Definition von $\Gamma_{u}$ und die Voraussetzungen des Lemmas existiert an jedem Punkt $y_{0} \in \overline{B_{d}} \cap \{u = \Gamma_{u}\}$ sowohl eine Gerade $L_{0}$ die $\Gamma_{u}$ bei $y_{0}$ von unten berührt, als auch eine Parabel die $\Gamma_{u}$ bei $y_{0}$ von oben berührt (zumindest in einer von $y_{0}$ unabhängigen $\epsilon$-Umgebung). Insgesamt also erhalten wir obere und untere Schranken an $\Gamma_{u}$,
    \[L_{0}(x) \leq \Gamma_{u}(x) \leq P(x) \quad \forall x \in \overline{B}_{\epsilon}(y_{0}), \quad L_{0}(y_{0}) = \Gamma_u(y_{0}) = P(y_{0})\] 
    was eine Annäherung an die Voraussetzungen in Proposition 2 ist und wir sehen, dass $L_{0}(y) = \Gamma_u(y_{0}) + \TD \Gamma_u(y_{0})(y-y_{0}) $ gilt.

    Mit Benutzung von Lemma 2 bleibt zu zeigen, dass $\Gamma_{u} \in C^{1,1}(\overline{B}_{d})$ und 
    \[\det \TD^{2} \Gamma_{u}(x) = 0 \quad \text{f.ü. auf }B_{d} \setminus \{u = \Gamma_{u}\}.\]
    Hierzu gehen wir in zwei Schritten vor.
    \begin{enumerate}[label = \arabic*.]
        \item Sei $x_{0} \in \overline{B}_{d} \setminus \{u = \Gamma_{u}\}$ und $L$ eine Stützhyperebene für $\Gamma_{u}$ bei $x_{0}$ in $\overline{B}_{2d}$, dann gilt:
        \begin{enumerate}[label = (\alph*)]
        \item $x_{0}$ ist in einem Simplex $S$ mit, nicht notwendigerweise verschiedenen, Kanten $x_{1},...,x_{n+1}$ (also $S = \text{konv}\{x_{1},...,x_{n+1}\}$) und $L = \Gamma_{u}$ auf diesem Simplex. Letzteres impliziert, dass $\det \TD^{2} \Gamma_{u} (x) = 0$ f.ü. auf $\overline{B}_{d} \setminus \{u = \Gamma_{u}\}$, womit der erste Teil bereits bewiesen ist. Weiterhin sind alle $x_{i} \in \overline{B}_{d} \cap \{u = \Gamma_{u}\}$, bis auf möglicherweise $x_{n+1}$, welches in $\partial B_{2d}$ liegen kann.
        \end{enumerate}
        Um dies zu zeigen, beweist man, dass für $M \coloneqq \{x \in \overline{B}_{2d} : L(x) = - u^{-}(x)\}$ $x_{0} \in \text{konv}(M)$ und wendet den Satz von Carathéodory auf $M$ an. Somit erhält man $x_{0}$ als eine Konvexkombination von Punkten $x_{1},...,x_{n+1} \in M$, also 
        \[x_{0} = \sum_{i=1}^{n+1} \lambda_{i} x_{i}, \quad \sum_{i=1}^{n+1} \lambda_{i} = 1, \quad \lambda_{i} \geq 0 \quad \forall i \in \{1,...,n+1\}.\]
        \begin{enumerate}[label = (\alph*)]
            \item[(b)] Für mindestens eins dieser $\lambda_{i}$ gilt $\lambda_{i} \geq \sfrac{1}{(3n)}$ und $x_{i} \in \overline{B}_{d} \cap \{u = \Gamma_{u}\}$.
        \end{enumerate}
        \item Sei $h \in \R^{n}$ mit $|h|<d$. Durch Umbenennung erhalten wir, dass das  $x_{i}$ aus (1b) $x_{1}$ ist und es gilt
            \[x_{0} + h = \lambda_{1}\left( x_{1} + \sfrac{h}{\lambda_{1}} \right) + \lambda_{2}x_{2} + ... + \lambda_{n+1} x_{n+1}. \]
            Benutzen wir die Konvexität von $\Gamma_u$ und die Definition von $L$ haben wir
        \[ L(x_{0}+h) \leq \Gamma_{u}(x_{0} + h) \leq \lambda_{1} \Gamma_u(x_{1} + \sfrac{h}{\lambda_{1}}) + \lambda_{2} x_{2} + ... + \lambda_{n+1} x_{n+1}.\]
        Nehmen wir zusätzlich an, dass $h < \sfrac{\epsilon}{n}$ gilt $\sfrac{|h|}{\lambda_{1}} < \epsilon$. Weiterhin haben wir das $L$ eine Stützhyperebene für $\Gamma_u$ bei $x_{1} \in \overline{B}_{d} \cap \{u= \Gamma_u\}$ ist und zusammen mit der vorausgesetzten Parabel erhalten wir
        \begin{align*}
            L(x_{0} + h) &\leq \Gamma_u(x_{0} + h) \leq \lambda_{1} P(x_{1} + \sfrac{h}{\lambda_{1}}) + \lambda_{2}L(x_{2}) + ... + \lambda_{n+1}L(x_{n+1})\\
                         &\leq \lambda_{1} \left( L(x_{1} + \sfrac{h}{\lambda_{1}}) + \sfrac{K}{2}|\sfrac{h}{\lambda_{1}}|^{2} \right) + \lambda_{2}L(x_{2}) + ... + \lambda_{n+1}L(x_{n+1})\\
                         &\leq L(x_{0} + h) + \sfrac{(3nK)}{2}|h|^{2}
        \end{align*}
    \end{enumerate}
        Somit haben wir nun für alle $x_{0} \in \overline{B}_{d}$ eine Abschätzung an $\Gamma_u$, von der Form
        \[P_{1}(x) \leq \Gamma_{u} (x) \leq P_{2}(x) \quad \forall x \in B_{\sfrac{\epsilon}{3n}}(x_{0}), \quad P_{1}(x_{0}) = \Gamma_u(x_{0}) = P_{2}(x_{0}),\]
    wobei $P_{1}(x) = L(x)$ bzw. $L_{0}(x)$ ist und $P_{2}(x) = l(x) + \sfrac{3nK}{2}|x_{0} -x|^{2}$. Nun erhalten wir mit Proposition 2, dass $\Gamma_u \in C^{1,1}(\overline{B}_{d})$.
\end{proof}

\begin{proof}[Beweis des Theorems]
    Mit Lemma 3 reicht es zu zeigen, dass für alle $x_{0} \in \overline{B}_{d} \cap \{u = \Gamma_{u}\}$ eine konvexe Parabel mit Öffnungsparameter $C(n, \lambda, \Lambda) \sup_{B_{d}}f^{+}$existiert der $x_{0}$ auf $B_{\nu d}(x_{0})$ (mit $\nu = \nu(n)<1$) von oben berührt und dass
    \[\det \TD^{2}\Gamma_u(x_{0}) \leq C(n, \lambda, \Lambda) f^{+}(x_{0})^{n} \quad \text{f.ü. auf } B_{d} \cap \{u = \Gamma_{u}\}.\]
    Sei nun $x_{0} \in \overline{B}_{d} \cap \{u = \Gamma_{u}\}$ und $L$ eine Stützhyperebene für $\Gamma_u$ bei $x_{0}$ und wendet man die Proposition 1 mit $\Omega = B_{d}, \Omega_{1} = B_{2d}, F= \mathcal M^{-},u=0,v=u,f=0,g = f$ erhält man, dass $-u^{-} = \inf(0,u) \in \overline{S}(\lambda,\Lambda,f^{+}\chi_{B_{d}})$ in $B_{2d}$.

    Da $L$ affin ist, ist  $\Gamma_u - L$ auch konvex und nach Satz 14. iii) aus dem vorletzten Vortrag ist $-u^{-} - L \in \overline{S}(\lambda, \Lambda, f^{+} \chi_{B_{d}})$ in $B_{2d}$. Weiterhin ist für ein $0 < \delta \leq d$ $x_{0} \in B_{\delta}(x_{0})$ und $0 \leq \Gamma_{u} - L \leq -u^{-} - L$ auf $B_{\delta}(x_{0})$ mit Gleichheit bei $x_{0}$. Mit Lemma 1 haben wir
    \[L(x) \leq \Gamma_u(x) \leq L(x) + C(n,\lambda,\Lambda)(\sup_{B_{\delta}(x_{0})} f^{+}\chi_{B_{d}})|x-x_{0}|^{2} \quad \forall x \in B_{\nu \delta}(x_{0}).\]
    Wir haben also die geforderte Parabel gefunden und für $\delta \to 0$ erhalten wir mit der Stetigkeit von $f$, dass $\TD^{2} \Gamma_{u}(x_{0})$ positiv semidefinit und $\TD^{2} \Gamma_{u}(x_{0}) - \text{id} C(n,\lambda, \Lambda)f^{+}(x_{0})$ negativ semidefinit ist. Also gilt für die Eigenwerte $\eta_{i}$ von $\TD^{2} \Gamma_u(x_{0})$, $0 \leq \eta_{i} \leq C(n, \lambda, \Lambda) f^{+}(x_{0})$ und da das Produkt der Eigenwerte die Determinante ist folgt die behauptete Abschätzung.
\end{proof}

Mit dem nun bewiesenen Theorem und der Proposition lässt sich die ABP Abschätzung auf beliebige beschränkte Gebiete verbessern.

\begin{kor}
    Sei $\Omega$ ein beschränktes Gebiet in $\R^{n}$ und $f$ stetig und beschränkt auf $\Omega$. Angenommen, dass $u \in \overline{S}(\lambda, \Lambda, f)$ in $\Omega$, $u$ stetig und $u \geq 0$ auf $\partial \Omega$, dann gilt
    \[\sup_{\Omega}u^{-} \leq C(n, \lambda, \Lambda) \ \mathrm{diam}(\Omega) ||f^{+}||_{\mathrm{L}^{n}(\Omega \cap \{u = \Gamma_{u}\})}.\] 
    Wobei $\Gamma_u$ die konvexe Hülle von $-u^{-}$ auf $B_{2d}$ ist und $B_{d}$ ein Ball mit Radius $d = \mathrm{diam}(\Omega)$, sodass $\Omega \subset B_{d}$ und $u \equiv 0$ außerhalb von $\Omega$. 
\end{kor}
\begin{proof}
    Proposition 1 und die ABP Abschätzung liefern das gewünschte Resultat. 
\end{proof}

\begin{kor}[Maximumsprinzip für Viskositätslösungen]
    Für ein stetiges $u \in C(\overline{\Omega})$ gilt:
    \begin{enumerate}[label = \arabic*.]
        \item $u \in \overline{S}(\lambda, \Lambda, 0)$ und $u \geq 0$ auf $\partial \Omega$, dann ist $u \geq 0$ auf $\Omega$.
        \item $u \in \underline{S}(\lambda, \Lambda, 0)$ und $u \leq 0$ auf $\partial \Omega$, dann ist $u \leq 0$ auf $\Omega$.
    \end{enumerate}
\end{kor}
\begin{proof}
    Wende das vorherige Korollar mit $f=0$ auf $u$ bzw. $-u$ an.
\end{proof}


\end{document}
